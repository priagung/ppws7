from django import forms
from .models import *

class UserInput(forms.Form):
    name = forms.CharField(label = 'Name', widget = forms.TextInput(), max_length=30)
    status = forms.CharField(label = 'Status', widget = forms.TextInput())
