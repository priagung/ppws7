from django.db import models
import datetime

# Create your models here.

class Status(models.Model):
    name = models.CharField(max_length = 255)
    status = models.CharField(max_length = 255)
    post_date = models.DateTimeField(auto_now_add=True)
