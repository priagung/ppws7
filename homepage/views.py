from django.shortcuts import render, redirect
from .models import *
from .forms import *

# Create your views here.
def index(request):
    statusList = Status.objects.all()
    statusForm = UserInput()
    formInput = UserInput(request.POST)
    
    if request.method == 'GET':
        return render(request, 'index.html', {'statusList' : statusList, 'statusForm': statusForm})

    if request.method == 'POST':
        if formInput.is_valid():
            input = Status(name = formInput.data['name'], status = formInput.data['status'])
            input.save()
            return render(request, 'index.html', {'statusList' : statusList, 'statusForm': statusForm})
        else:
            return render(request, 'index.html', {'statusList' : statusList, 'statusForm': statusForm})


def confirmation(request):
    if request.method == 'POST':
        formInput = UserInput(request.POST)
        if formInput.is_valid():
            confirmName = formInput.data['name']
            confirmStatus = formInput.data['status']
            confirmForm = UserInput(initial={'name':confirmName, 'status':confirmStatus})
            return render(request, 'confirmation.html', {'confirmForm' : confirmForm})
        else:
            response = redirect('/')
            return response
    if request.method == 'GET':
        return redirect('/')