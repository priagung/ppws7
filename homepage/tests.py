from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from django.apps import apps
from homepage.apps import HomepageConfig
from .forms import *
from .views import *
from .models import *
from selenium import webdriver
import unittest
import time


# Create your tests here.

class UnitTest(TestCase):
	def testHomepage(self):
		response = self.client.get('/')
		self.assertEquals(response.status_code, 200)
		
	def testCorrectTemplate(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')
	
	def testHomepageExists(self):
		response = Client().get('/')
		self.assertEquals(response.status_code, 200)

	def testConfirmationPageExist(self):
		response = Client().get('/confirmation/')
		self.assertEquals(response.status_code, 302)
		#moved temporarily HTTP302


	
	def testIndexFunction(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def testConfirmationFunction(self):
		found = resolve('/confirmation/')
		self.assertEqual(found.func, confirmation)


	def testModelCreate(self):
		testModel = Status.objects.create(name = "Pria", status = "Tryhard")
		self.assertTrue(isinstance(testModel, Status))
		statusCount = Status.objects.all().count()
		self.assertEqual(statusCount , 1)



class TestFunctional(LiveServerTestCase):
	def setUp(self):
		super().setUp()
		chrome_options = webdriver.ChromeOptions()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('--disable-gpu')
		chrome_options.add_argument('--disable-dev-shm-usage')
		self.driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='chromedriver')

	def tearDown(self):
		self.driver.quit()

	def testConfirmingYes(self):
		self.driver.get(self.live_server_url)
		time.sleep(5)

		name_box = self.driver.find_element_by_name('name')
		name_box.send_keys('Pria')

		status_box = self.driver.find_element_by_name('status')
		status_box.send_keys('Mantap')

		submit_button = self.driver.find_element_by_name('post')
		submit_button.click()
		time.sleep(5)

		save_button = self.driver.find_element_by_name('yes')
		save_button.click()
		time.sleep(5)
	
		self.assertIn('Pria', self.driver.page_source)
		self.assertIn('Mantap', self.driver.page_source)

	def testConfirmingNo(self):
		self.driver.get(self.live_server_url)
		time.sleep(5)

		name_box = self.driver.find_element_by_name('name')
		name_box.send_keys('Pria')
		status_box = self.driver.find_element_by_name('status')
		status_box.send_keys('Toxic')
		submit_button = self.driver.find_element_by_name('post')
		submit_button.click()
		time.sleep(5)

		save_button = self.driver.find_element_by_name('back')
		save_button.click()
		time.sleep(5)

		self.assertNotIn('Pria', self.driver.page_source)
		self.assertNotIn('Toxic', self.driver.page_source)
